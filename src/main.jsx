import React from "react";
import ReactDOM from "react-dom/client";
import App from "@/App";
import "@/assets/scss/app.scss";
import "swiper/css";
import "swiper/css/free-mode";

ReactDOM.createRoot(document.getElementById("root")).render(<App />);
