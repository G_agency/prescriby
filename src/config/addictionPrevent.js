import { ReactComponent as Icon1 } from "@/assets/svg/icon-1.svg";
import { ReactComponent as Icon2 } from "@/assets/svg/icon-2.svg";
import { ReactComponent as Icon3 } from "@/assets/svg/icon-3.svg";

const addictionPrevent = [
  {
    icon: Icon1,
    title:
      "Currently the only <b>treatment plans</b> made by healthcare professionals are non-digital and time consuming",
    image: "/images/image-1.png",
    description:
      "Prescriby enables the creation of personalized data driven treatment plans in seconds",
    center: false,
  },
  {
    icon: Icon2,
    title:
      "The patient lacks <b>education</b> and tools to be an active member of the treatment",
    image: "/images/image-2.png",
    description:
      "Prescriby enables patient engagement and the possibilty of safer treatments",
    center: true,
  },
  {
    icon: Icon3,
    title:
      "Healthcare professionals lack the data needed for <b>close monitoring</b> and early intervention",
    image: "/images/image-3.png",
    description:
      "Prescriby enables efficient monitoring of large cohort of patients and intervention when necessary\n",
    center: false,
  },
];

export default addictionPrevent;
