import { ReactComponent as Icon4 } from "@/assets/svg/icon-4.svg";
import { ReactComponent as Icon5 } from "@/assets/svg/icon-5.svg";
import { ReactComponent as Icon6 } from "@/assets/svg/icon-6.svg";
import { ReactComponent as Icon7 } from "@/assets/svg/icon-7.svg";

const offer = [
  {
    name: "Data driven treatment plans",
    icon: Icon4,
  },
  {
    name: "Prevent overprescribing",
    icon: Icon5,
  },
  {
    name: "Increased patient throughput",
    icon: Icon6,
  },
  {
    name: "Real time data for safer follow up",
    icon: Icon7,
  },
];

export default offer;
