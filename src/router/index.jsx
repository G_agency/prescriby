import React from "react";
import { createBrowserRouter } from "react-router-dom";
import Main from "@/views/main/Main.jsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Main />,
  },
]);

export default router;
