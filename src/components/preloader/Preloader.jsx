import React, { useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "class-names";
import "./preloader.scss";
import images from "@/config/images.js";

function Preloader({ className = "", loaded = () => {} }) {
  const loadImage = (imageUrl) => {
    return new Promise((resolve, reject) => {
      const image = new Image();

      image.src = imageUrl;

      image.onload = () => {
        resolve(image.src);
      };

      image.onerror = reject;
    });
  };

  useEffect(() => {
    Promise.all(images.map(loadImage)).finally(loaded);
  }, []);

  return (
    <div className={classNames(className, "preloader")}>
      <ul className="loader">
        <li className="center" />
        {Array.from({ length: 8 }).map((_, index) => (
          <li key={index} className={classNames("item", `item-${index + 1}`)} />
        ))}
      </ul>
    </div>
  );
}

Preloader.propTypes = {
  className: PropTypes.string,
  loaded: PropTypes.func,
};

export default Preloader;
