import React from "react";
import "./offer-card.scss";
import PropTypes from "prop-types";
import classNames from "class-names";

function OfferCard({ className = "", icon, name }) {
  return (
    <div className={classNames(className, "offer-card")}>
      {React.createElement(icon, {
        className: "offer-card__icon",
      })}
      <p className="offer-card__name">{name}</p>
    </div>
  );
}

OfferCard.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
};

export default OfferCard;
