import React from "react";
import PropTypes from "prop-types";
import classNames from "class-names";
import "./addiction-prevent-card.scss";

function AddictionPreventCard({ className = "", icon, title, center }) {
  return (
    <div
      className={classNames(className, "addiction-prevent-card", {
        "addiction-prevent-card--center": center,
      })}
    >
      <div className="addiction-prevent-card__icon-plate">
        {React.createElement(icon)}
      </div>
      <p
        className="addiction-prevent-card__title"
        dangerouslySetInnerHTML={{ __html: title }}
      />
    </div>
  );
}

AddictionPreventCard.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  center: PropTypes.bool,
};

export default AddictionPreventCard;
