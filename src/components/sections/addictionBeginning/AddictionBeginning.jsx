import React from "react";
import { ReactComponent as GreenPoints } from "@/assets/svg/decor/green-points.svg";
import "./addiction-beginning.scss";
import useAnimation from "./useAnimation.js";
import useGlobal from "@/store/useGlobal.js";

function AddictionBeginning() {
  const {
    textRef,
    text2Ref,
    text3Ref,
    pointsRef,
    points2Ref,
    imageRef,
    image2Ref,
  } = useAnimation();

  const { isMobile } = useGlobal();

  return (
    <section className="container addiction-beginning">
      <img
        data-speed="0.9"
        src="/images/points-white.png"
        className="addiction-beginning__float addiction-beginning__float--points-white"
      />
      <img
        data-speed="0.9"
        src="/images/points-white.png"
        className="addiction-beginning__float addiction-beginning__float--points-white-2"
      />

      <img
        data-speed="1.1"
        src="/images/decor-white.png"
        className="addiction-beginning__float addiction-beginning__float--white"
      />
      <img
        data-speed="1.05"
        src="/images/decor-blue-rounded.png"
        className="addiction-beginning__float addiction-beginning__float--blue-border"
      />
      <img
        data-speed="1"
        src="/images/decor-blue-rounded.png"
        className="addiction-beginning__float addiction-beginning__float--blue-border-2"
      />
      <img
        data-speed="1"
        src="/images/decor-green-rounded.png"
        className="addiction-beginning__float addiction-beginning__float--green-border"
      />
      <img
        data-speed="0.95"
        src="/images/decor-green.png"
        className="addiction-beginning__float addiction-beginning__float--green"
      />

      <img
        src="/images/decor-green-rounded.png"
        className="addiction-beginning__float addiction-beginning__float--green-border-2"
      />
      <img
        data-speed="0.9"
        src="/images/decor-green.png"
        className="addiction-beginning__float addiction-beginning__float--green-2"
      />
      <img
        data-speed="0.95"
        src="/images/decor-blue-rounded.png"
        className="addiction-beginning__float addiction-beginning__float--blue-border-3"
      />

      <div className="addiction-beginning__title">
        <h2 className="title title--slim" ref={textRef}>
          <u className="title__underline">The unseen beginning</u>
          <span>of addiction</span>
        </h2>
      </div>
      <div className="addiction-beginning__grid">
        <div className="addiction-beginning__image-wrapper" ref={imageRef}>
          <img
            className="addiction-beginning__image"
            src="/images/grid-image-1.png"
          />
          <img
            data-speed="1.1"
            className="addiction-beginning__decor addiction-beginning__decor--round"
            src="/images/grid-image-decor-1.png"
          />
          <img
            data-speed="0.95"
            className="addiction-beginning__decor addiction-beginning__decor--plate"
            src="/images/grid-image-plate-1.png"
          />
        </div>
        <div data-speed="0.95">
          <div ref={pointsRef}>
            <GreenPoints
              data-speed="0.95"
              className="addiction-beginning__green-points"
            />
          </div>
          <p className="addiction-beginning__text" ref={text2Ref}>
            {isMobile ? (
              <>
                <span>Healthcare authorities recommend</span>
                <span>that patients prescribed with addictive medication</span>
                <span>
                  receive
                  <b>education, close monitoring</b> and a
                </span>
                <span>
                  <b>treatment plan</b> to prevent the development of
                </span>
                <span>addiction. But today, healthcare professionals</span>
                <span>lack the tools and time to fulfill these</span>
                <span>requirements.</span>
              </>
            ) : (
              <>
                <span>Healthcare authorities recommend that patients</span>
                <span>prescribed with addictive medication receive</span>
                <span>
                  <b>education, close monitoring</b> and a <b>treatment plan</b>{" "}
                  to
                </span>
                <span>prevent the development of addiction. But today,</span>
                <span>healthcare professionals lack the tools and time to</span>
                <span>fulfill these requirements.</span>
              </>
            )}
          </p>
        </div>
      </div>
      <div className="addiction-beginning__grid addiction-beginning__grid--reverse">
        <div className="addiction-beginning__image-wrapper" ref={image2Ref}>
          <img
            className="addiction-beginning__image"
            src="/images/grid-image-2.png"
          />
          <img
            data-speed="1.1"
            className="addiction-beginning__decor addiction-beginning__decor--round"
            src="/images/grid-image-decor-2.png"
          />
          <img
            data-speed="0.95"
            className="addiction-beginning__decor addiction-beginning__decor--plate"
            src="/images/grid-image-plate-2.png"
          />
        </div>
        <div data-speed="0.95">
          <div ref={points2Ref}>
            <GreenPoints
              data-speed="0.95"
              className="addiction-beginning__green-points"
            />
          </div>

          <p className="addiction-beginning__text" ref={text3Ref}>
            {isMobile ? (
              <>
                <span>This leaves patients stranded alone with a</span>
                <span>prescription to a highly addictive medication </span>
                <span>and no followup other than their regular</span>
                <span>prescription renewals. The problem often lays</span>
                <span>low for months until the patient is diagnosed</span>
                <span>with clinical addiction.</span>
              </>
            ) : (
              <>
                <span>This leaves patients stranded alone with a</span>
                <span>
                  prescription to a highly addictive medication and no
                </span>
                <span>followup other than their regular prescription</span>
                <span>renewals. The problem often lays low for months </span>
                <span>
                  until the patient is diagnosed with clinical addiction.
                </span>
              </>
            )}
          </p>
        </div>
      </div>
    </section>
  );
}

export default AddictionBeginning;
