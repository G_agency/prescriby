import { useEffect, useRef } from "react";
import gsap from "@/lib/gsap";
import ScrollTrigger from "@/lib/gsap/ScrollTrigger";
import SplitText from "@/lib/gsap/SplitText";
import useGlobal from "@/store/useGlobal.js";
import { element } from "prop-types";

gsap.registerPlugin(SplitText, ScrollTrigger);

const useAnimation = () => {
  const { isMobile } = useGlobal();

  const textRef = useRef(null);
  const text2Ref = useRef(null);
  const text3Ref = useRef(null);
  const splitTextRef = useRef(null);
  const splitText2Ref = useRef(null);
  const splitText3Ref = useRef(null);

  const pointsRef = useRef(null);
  const points2Ref = useRef(null);

  const imageRef = useRef(null);
  const image2Ref = useRef(null);

  useEffect(() => {
    splitTextRef.current = new SplitText(textRef.current, { type: "lines" });
    splitText2Ref.current = new SplitText(text2Ref.current, { type: "lines" });
    splitText3Ref.current = new SplitText(text3Ref.current, { type: "lines" });

    const initTitle = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(splitTextRef.current.lines, {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        duration,
      });
    };

    const initBlock = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(splitText2Ref.current.lines, {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        textAlign: "left",
        duration,
      });

      gsap.to(imageRef.current, {
        translateY: 50,
        opacity: 0,
        duration,
      });

      gsap.to(pointsRef.current, {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        duration,
      });
    };

    const initBlock2 = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(splitText3Ref.current.lines, {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        textAlign: "left",
        duration,
      });

      gsap.to(image2Ref.current, {
        translateY: 50,
        opacity: 0,
        duration,
      });

      gsap.to(points2Ref.current, {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        duration,
      });
    };

    initTitle(true);
    initBlock(true);
    initBlock2(true);

    gsap.set(".addiction-beginning__float", {
      opacity: 0,
      stagger: 0.1,
    });

    ScrollTrigger.create({
      trigger: textRef.current,
      start: "top 80%",
      onLeaveBack() {
        initTitle(false);
      },
      onEnter() {
        gsap.to(splitTextRef.current.lines, {
          translateY: "0%",
          rotate: "0deg",
          opacity: 1,
          duration: 0.3,
          stagger: 0.1,
        });
      },
    });

    document
      .querySelectorAll(".addiction-beginning__float")
      .forEach((element) => {
        ScrollTrigger.create({
          trigger: element,
          start: "top 80%",
          onLeaveBack() {
            gsap.to(element, { opacity: 0, duration: 0.3 });
          },
          onEnter() {
            gsap.to(element, { opacity: 1, duration: 0.3 });
          },
        });
      });

    ScrollTrigger.create({
      trigger: text2Ref.current,
      start: "top 90%",
      onLeaveBack() {
        initBlock(false);
      },
      onEnter() {
        const timeline = gsap.timeline();

        timeline.to(
          imageRef.current,
          { translateY: 0, opacity: 1, duration: 0.7 },
          "<0.1"
        );

        timeline.to(
          pointsRef.current,
          { translateY: "0%", rotate: "0deg", opacity: 1, duration: 0.3 },
          "<0.1"
        );

        splitText2Ref.current.lines.forEach((line) => {
          timeline.to(
            line,
            { translateY: "0%", rotate: "0deg", opacity: 1, duration: 0.3 },
            "<0.1"
          );
        });
      },
    });

    ScrollTrigger.create({
      trigger: text3Ref.current,
      start: "top 90%",
      onLeaveBack() {
        initBlock2(false);
      },
      onEnter() {
        const timeline = gsap.timeline();

        timeline.to(
          image2Ref.current,
          { translateY: 0, opacity: 1, duration: 0.7 },
          "<0.1"
        );

        timeline.to(
          points2Ref.current,
          { translateY: "0%", rotate: "0deg", opacity: 1, duration: 0.3 },
          "<0.1"
        );

        splitText3Ref.current.lines.forEach((line) => {
          timeline.to(
            line,
            { translateY: "0%", rotate: "0deg", opacity: 1, duration: 0.3 },
            "<0.1"
          );
        });
      },
    });
  }, []);

  return {
    textRef,
    text2Ref,
    text3Ref,
    pointsRef,
    points2Ref,
    imageRef,
    image2Ref,
  };
};

export default useAnimation;
