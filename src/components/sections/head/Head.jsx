import React from "react";
import { ReactComponent as Logo } from "@/assets/svg/prescriby.svg";
import "./head.scss";
import useAnimation from "./useAnimation.js";

function Head() {
  const { textRef } = useAnimation();

  const onClickButton = () => {
    window.scrollTo({
      top: document
        .querySelector(".addiction-beginning__title")
        .getBoundingClientRect().y,
    });
  };

  return (
    <main className="container head">
      <div className="head__content">
        <Logo className="head__logo" />
        <div className="head__title">
          <h1 className="title" ref={textRef}>
            Prescriby is the <u className="title__underline">gold standard</u>{" "}
            of safe prescriptions
          </h1>
        </div>
        <p className="head__description">
          Prevent addiction while saving valuable time for healthcare
          professionals
        </p>
        <button onClick={onClickButton} className="head__button">
          <span className="head__button-inner">Get Started</span>
        </button>
      </div>

      <img
        data-speed="1.5"
        src="/images/decor-white.png"
        className="head__decor head__decor--white"
      />
      <img
        data-speed="1.3"
        className="head__decor head__decor--background"
        src="/images/head-background.png"
      />
      <img
        data-speed="1.1"
        className="head__decor head__decor--mac"
        src="/images/mac.png"
      />
      <img
        data-speed="1.05"
        className="head__decor head__decor--phone-screen"
        src="/images/phone-screen.png"
      />

      <img
        data-speed="1"
        src="/images/decor-green-rounded.png"
        className="head__decor head__decor--green-border-top"
      />
      <img
        data-speed="1"
        src="/images/decor-blue-rounded.png"
        className="head__decor head__decor--blue-border-top"
      />
      <img
        data-speed="1"
        src="/images/decor-green.png"
        className="head__decor head__decor--green-top"
      />

      <img
        data-speed="1.05"
        src="/images/decor-blue-rounded.png"
        className="head__decor head__decor--blue-border-bottom"
      />
      <img
        data-speed="1.15"
        src="/images/decor-green-rounded.png"
        className="head__decor head__decor--green-border-bottom"
      />
      <img
        data-speed="1.2"
        src="/images/decor-green.png"
        className="head__decor head__decor--green-bottom"
      />
    </main>
  );
}

export default Head;
