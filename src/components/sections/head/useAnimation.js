import { useEffect, useRef } from "react";
import gsap from "@/lib/gsap";
import SplitText from "@/lib/gsap/SplitText";
import useGlobal from "@/store/useGlobal.js";

gsap.registerPlugin(SplitText);

const useAnimation = () => {
  const { isMobile, isLoaded } = useGlobal();

  const textRef = useRef(null);
  const splitTextRef = useRef(null);

  const startAnimation = () => {
    const timeline = gsap.timeline();

    timeline.to(
      ".head__logo",
      { translateY: "none", rotate: "0deg", opacity: 1, duration: 0.3 },
      "<0.1"
    );

    timeline.to(
      ".head__decor",
      { opacity: 1, duration: 0.3, stagger: 0.1 },
      "<0.1"
    );

    splitTextRef.current.lines.forEach((line) => {
      timeline.to(
        line,
        { translateY: "0%", rotate: "0deg", opacity: 1, duration: 0.3 },
        "<0.1"
      );
    });

    timeline.to(
      ".head__description",
      { translateY: "0%", rotate: "0deg", opacity: 1, duration: 0.3 },
      "<0.1"
    );

    timeline.to(
      ".head__button",
      { translateY: "0%", rotate: "0deg", opacity: 1, duration: 0.3 },
      "<0.1"
    );
  };

  useEffect(() => {
    if (!isLoaded) return;

    splitTextRef.current = new SplitText(textRef.current, { type: "lines" });

    gsap.set(".head__logo", {
      translateY: "100%",
      rotate: "5deg",
      opacity: 0,
    });

    gsap.set(".head__decor", {
      opacity: 0,
    });

    gsap.set(splitTextRef.current.lines, {
      translateY: "100%",
      rotate: "5deg",
      opacity: 0,
      textAlign: isMobile ? null : "left",
    });

    gsap.set(".head__description", {
      translateY: "100%",
      rotate: "5deg",
      opacity: 0,
      duration: 0.3,
    });

    gsap.set(".head__button", {
      translateY: "100%",
      rotate: "5deg",
      opacity: 0,
      duration: 0.3,
    });

    setTimeout(() => {
      startAnimation();
    }, 500);
  }, [isLoaded]);

  return {
    textRef,
  };
};

export default useAnimation;
