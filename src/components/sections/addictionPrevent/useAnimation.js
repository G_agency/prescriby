import { useEffect, useRef } from "react";
import gsap from "@/lib/gsap";
import SplitText from "@/lib/gsap/SplitText";
import ScrollTrigger from "@/lib/gsap/ScrollTrigger";
import useGlobal from "@/store/useGlobal.js";

gsap.registerPlugin(SplitText, ScrollTrigger);

const useAnimation = () => {
  const { isMobile } = useGlobal();

  const textRef = useRef(null);
  const splitTextRef = useRef(null);

  useEffect(() => {
    splitTextRef.current = new SplitText(textRef.current, { type: "lines" });

    const initTitle = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(splitTextRef.current.lines, {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        duration,
      });
    };

    const initCards = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(".addiction-prevent__cards--desktop", {
        translateY: 50,
        opacity: 0,
        duration,
      });

      gsap.to(".addiction-prevent__cards-button--desktop", {
        opacity: 0,
        duration,
      });
    };

    const initCardsMobile = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(".addiction-prevent__card--mobile", {
        translateY: 50,
        opacity: 0,
        duration,
      });

      gsap.to(".addiction-prevent__cards-button--mobile", {
        opacity: 0,
        duration,
      });
    };

    const initImages = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(".addiction-prevent__arrow--desktop", {
        translateY: 50,
        opacity: 0,
        duration,
      });

      gsap.to(".addiction-prevent__image-wrapper--desktop", {
        translateY: 50,
        opacity: 0,
        duration,
      });

      gsap.to(".addiction-prevent__description--desktop", {
        translateY: 50,
        opacity: 0,
        duration,
      });
    };

    const initImagesMobile = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(".addiction-prevent__arrow--mobile", {
        translateY: 50,
        opacity: 0,
        duration,
      });

      gsap.to(".addiction-prevent__card-decor", {
        opacity: 0,
        duration,
      });

      gsap.to(".addiction-prevent__image-wrapper--mobile", {
        translateY: 50,
        opacity: 0,
        duration,
      });

      gsap.to(".addiction-prevent__description--mobile", {
        translateY: 50,
        opacity: 0,
        duration,
      });
    };

    initTitle(true);
    if (!isMobile) {
      initCards(true);
      initImages(true);
    } else {
      initCardsMobile(false);
      initImagesMobile(false);
    }

    ScrollTrigger.create({
      trigger: textRef.current,
      start: "top 80%",
      onLeaveBack() {
        initTitle(false);
      },
      onEnter() {
        gsap.to(splitTextRef.current.lines, {
          translateY: "0%",
          rotate: "0deg",
          opacity: 1,
          duration: 0.3,
          stagger: 0.1,
        });
      },
    });

    if (!isMobile) {
      ScrollTrigger.create({
        trigger: ".addiction-prevent__cards--desktop",
        start: "top 80%",
        onLeaveBack() {
          initCards(false);
        },
        onEnter() {
          const timeline = gsap.timeline();

          timeline.to(
            ".addiction-prevent__cards--desktop",
            {
              translateY: 0,
              opacity: 1,
              duration: 0.7,
            },
            "<0.1"
          );

          timeline.to(
            ".addiction-prevent__cards-button--desktop",
            {
              opacity: 1,
              duration: 0.7,
            },
            "0.4"
          );
        },
      });

      ScrollTrigger.create({
        trigger: ".addiction-prevent__image-container--center",
        start: "top 80%",
        onLeaveBack() {
          initImages(false);
        },
        onEnter() {
          gsap.to(".addiction-prevent__arrow--desktop", {
            translateY: 0,
            opacity: 1,
            duration: 0.7,
            stagger: 0.2,
          });

          gsap.to(".addiction-prevent__image-wrapper--desktop", {
            translateY: 0,
            opacity: 1,
            duration: 0.7,
            stagger: 0.2,
            delay: 0.3,
          });

          gsap.to(".addiction-prevent__description--desktop", {
            translateY: 0,
            opacity: 1,
            duration: 0.7,
            stagger: 0.2,
            delay: 0.5,
          });
        },
      });
    } else {
      const buttons = document.querySelectorAll(
        ".addiction-prevent__cards-button--mobile"
      );

      document
        .querySelectorAll(".addiction-prevent__card--mobile")
        .forEach((card, index) => {
          ScrollTrigger.create({
            trigger: card,
            start: "top 80%",
            onLeaveBack() {
              gsap.to(card, {
                translateY: 50,
                opacity: 0,
                duration: 0.3,
              });

              gsap.to(buttons[index], {
                opacity: 0,
                duration: 0.3,
              });
            },
            onEnter() {
              const timeline = gsap.timeline();

              gsap.to(
                card,
                {
                  translateY: 0,
                  opacity: 1,
                  duration: 0.7,
                },
                "<0.1"
              );

              timeline.to(
                buttons[index],
                {
                  opacity: 1,
                  duration: 0.7,
                },
                "0.4"
              );
            },
          });
        });

      const decors = document.querySelectorAll(
        ".addiction-prevent__card-decor"
      );
      const imageWrappers = document.querySelectorAll(
        ".addiction-prevent__image-wrapper--mobile"
      );
      const descriptions = document.querySelectorAll(
        ".addiction-prevent__description--mobile"
      );

      document
        .querySelectorAll(".addiction-prevent__arrow--mobile")
        .forEach((arrow, index) => {
          ScrollTrigger.create({
            trigger: arrow,
            start: "top 90%",
            onLeaveBack() {
              gsap.to(arrow, {
                translateY: 50,
                opacity: 0,
                duration: 0.3,
              });

              gsap.to(decors[index], {
                opacity: 0,
                duration: 0.3,
              });

              gsap.to(imageWrappers[index], {
                translateY: 50,
                opacity: 0,
                duration: 0.3,
              });

              gsap.to(descriptions[index], {
                translateY: 50,
                opacity: 0,
                duration: 0.3,
              });
            },
            onEnter() {
              gsap.to(arrow, {
                translateY: 0,
                opacity: 1,
                duration: 0.7,
                stagger: 0.2,
              });

              gsap.to(decors[index], {
                opacity: 1,
                duration: 0.3,
              });

              gsap.to(imageWrappers[index], {
                translateY: 0,
                opacity: 1,
                duration: 0.7,
                stagger: 0.2,
                delay: 0.3,
              });

              gsap.to(descriptions[index], {
                translateY: 0,
                opacity: 1,
                duration: 0.7,
                stagger: 0.2,
                delay: 0.5,
              });
            },
          });
        });
    }
  }, []);

  return {
    textRef,
  };
};

export default useAnimation;
