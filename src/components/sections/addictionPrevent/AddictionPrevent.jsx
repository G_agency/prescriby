import React from "react";
import "./addiction-prevent.scss";
import addictionPrevent from "@/config/addictionPrevent.js";
import AddictionPreventCard from "@/components/cards/addictionPreventCard/AddictionPreventCard.jsx";
import { ReactComponent as ArrowBottom } from "@/assets/svg/decor/arrow-bottom.svg";
import { ReactComponent as ArrowBottomShort } from "@/assets/svg/decor/arrow-bottom-short.svg";
import classNames from "class-names";
import useAnimation from "@/components/sections/addictionPrevent/useAnimation.js";

function AddictionPrevent() {
  const { textRef } = useAnimation();
  return (
    <section className="container addiction-prevent">
      <img
        data-speed="1.1"
        src="/images/addiction-background.png"
        className="addiction-prevent__decor addiction-prevent__decor--background"
      />
      <img
        data-speed="1.05"
        src="/images/decor-white.png"
        className="addiction-prevent__decor addiction-prevent__decor--white"
      />
      <div className="addiction-prevent__content">
        <div className="addiction-prevent__title">
          <h2 className="title title--slim" ref={textRef}>
            <u className="title__underline">Why isn’t addiction </u>
            prevented from the start?
          </h2>
        </div>
        <div className="addiction-prevent__cards addiction-prevent__cards--desktop">
          {addictionPrevent.map((addictionPreventCard, index) => (
            <AddictionPreventCard
              className="addiction-prevent__card"
              key={index}
              title={addictionPreventCard.title}
              icon={addictionPreventCard.icon}
              center={addictionPreventCard.center}
            />
          ))}
        </div>
        <div
          data-speed="1.02"
          className="addiction-prevent__cards-button addiction-prevent__cards-button--desktop"
        >
          <span className="addiction-prevent__cards-button-inner">
            The Prescriby system
          </span>
        </div>
        <div className="addiction-prevent__images addiction-prevent__images--desktop">
          {addictionPrevent.map((addictionPreventCard, index) => (
            <div
              key={index}
              className={classNames("addiction-prevent__image-container", {
                "addiction-prevent__image-container--center":
                  addictionPreventCard.center,
              })}
            >
              {addictionPreventCard.center ? (
                <ArrowBottomShort className="addiction-prevent__arrow addiction-prevent__arrow--desktop" />
              ) : (
                <ArrowBottom className="addiction-prevent__arrow addiction-prevent__arrow--desktop" />
              )}
              <div className="addiction-prevent__image-wrapper addiction-prevent__image-wrapper--desktop">
                <img
                  src={addictionPreventCard.image}
                  className="addiction-prevent__image"
                />
              </div>
              <p className="addiction-prevent__description addiction-prevent__description--desktop">
                {addictionPreventCard.description}
              </p>
            </div>
          ))}
        </div>
        <div className="addiction-prevent__cards addiction-prevent__cards--mobile">
          {addictionPrevent.map((addictionPreventCard, index) => (
            <div
              key={index}
              className={classNames("addiction-prevent__card-wrapper", {
                "addiction-prevent__card-wrapper--center":
                  addictionPreventCard.center,
              })}
            >
              <img
                src="/images/offer-background-mobile.png"
                className="addiction-prevent__card-decor"
              />
              <AddictionPreventCard
                className="addiction-prevent__card addiction-prevent__card--mobile"
                key={index}
                title={addictionPreventCard.title}
                icon={addictionPreventCard.icon}
              />
              <div className="addiction-prevent__cards-button addiction-prevent__cards-button--mobile ">
                <span className="addiction-prevent__cards-button-inner">
                  The Prescriby system
                </span>
              </div>
              <ArrowBottomShort className="addiction-prevent__arrow addiction-prevent__arrow--mobile" />
              <div className="addiction-prevent__image-wrapper addiction-prevent__image-wrapper--mobile">
                <img
                  src={addictionPreventCard.image}
                  className="addiction-prevent__image"
                />
              </div>
              <p className="addiction-prevent__description addiction-prevent__description--mobile">
                {addictionPreventCard.description}
              </p>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}

export default AddictionPrevent;
