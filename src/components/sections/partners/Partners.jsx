import React from "react";
import "./partners.scss";
import classNames from "class-names";
import PropTypes from "prop-types";
import useAnimation from "@/components/sections/partners/useAnimation.js";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, FreeMode } from "swiper";

function Partners({ className = "" }) {
  useAnimation();

  return (
    <section className={classNames(className, "partners")}>
      <div className="partners__title">
        <h2 className="title title--slim">
          <u className="title__underline">Partners and programs</u>
        </h2>
      </div>
      <div className="partners__content">
        <div className="container partners__container">
          <div className="partners__decor" />
          <div className="partners__images">
            <Swiper
              autoplay={{
                delay: 0,
                disableOnInteraction: false,
              }}
              grabCursor
              speed={10000}
              freeMode
              modules={[Autoplay, FreeMode]}
              loop
              loopAdditionalSlides={3}
              slidesPerView="auto"
              className="partners__swiper"
            >
              {Array.from({ length: 6 }).map((_, index) => (
                <SwiperSlide key={index} className="partners__slide">
                  <img
                    src="/images/partners/partner-1.png"
                    className="partners__image"
                  />
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
        </div>
      </div>
    </section>
  );
}

Partners.propTypes = {
  className: PropTypes.string,
};

export default Partners;
