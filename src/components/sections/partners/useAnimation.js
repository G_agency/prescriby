import { useEffect } from "react";
import gsap from "@/lib/gsap";
import ScrollTrigger from "@/lib/gsap/ScrollTrigger";
import SplitText from "@/lib/gsap/SplitText";
import useGlobal from "@/store/useGlobal.js";

gsap.registerPlugin(SplitText, ScrollTrigger);

const useAnimation = () => {
  const { isMobile } = useGlobal();

  useEffect(() => {
    const initBlock = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(".partners__title", {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        duration,
      });

      gsap.to(".partners__image", {
        translateY: "40%",
        rotate: "5deg",
        opacity: 0,
        duration,
      });
    };

    initBlock(true);

    ScrollTrigger.create({
      trigger: ".partners__title",
      start: "top 80%",
      onLeaveBack: () => {
        initBlock(false);
      },
      onEnter() {
        gsap.to(".partners__image", {
          translateY: "0%",
          rotate: "0deg",
          opacity: 1,
          duration: 0.3,
          stagger: 0.05,
        });

        gsap.to(".partners__title", {
          translateY: "0%",
          rotate: "0deg",
          opacity: 1,
          duration: 0.3,
        });
      },
    });
  }, []);
};

export default useAnimation;
