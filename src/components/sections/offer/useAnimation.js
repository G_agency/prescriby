import { useEffect } from "react";
import gsap from "@/lib/gsap";
import ScrollTrigger from "@/lib/gsap/ScrollTrigger";
import SplitText from "@/lib/gsap/SplitText";
import useGlobal from "@/store/useGlobal.js";

gsap.registerPlugin(SplitText, ScrollTrigger);

const useAnimation = () => {
  const { isMobile } = useGlobal();

  useEffect(() => {
    const initBlock = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(".offer__title", {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        duration,
      });

      gsap.to(".offer__float", {
        opacity: 0,
        duration,
      });

      gsap.to(".offer__decor", {
        opacity: 0,
        duration,
      });
    };

    const initCards = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(".offer__card", {
        translateY: 50,
        rotate: "5deg",
        opacity: 0,
        duration,
      });
    };

    initBlock(true);
    initCards(true);

    ScrollTrigger.create({
      trigger: ".offer__title",
      start: "top 80%",
      onLeaveBack: () => {
        initBlock(false);
      },
      onEnter() {
        gsap.to(".offer__float", {
          opacity: 1,
          duration: 0.7,
          stagger: 0.2,
        });

        gsap.to(".offer__decor", {
          opacity: 1,
          duration: 0.7,
        });

        gsap.to(".offer__title", {
          translateY: "0%",
          rotate: "0deg",
          opacity: 1,
          duration: 0.3,
        });
      },
    });

    ScrollTrigger.create({
      trigger: ".offer__cards",
      start: "top 80%",
      onLeaveBack: () => {
        initCards(false);
      },
      onEnter() {
        gsap.to(".offer__card", {
          translateY: 0,
          rotate: "0deg",
          opacity: 1,
          duration: 0.3,
          stagger: 0.1,
        });
      },
    });
  }, []);
};

export default useAnimation;
