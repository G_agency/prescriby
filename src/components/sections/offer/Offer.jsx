import React from "react";
import "./offer.scss";
import offer from "@/config/offer.js";
import OfferCard from "@/components/cards/offerCard/OfferCard.jsx";
import useAnimation from "@/components/sections/offer/useAnimation.js";

function Offer() {
  useAnimation();

  return (
    <section className="container offer">
      <img
        data-speed="1.05"
        src="/images/offer-background.png"
        className="offer__decor"
      />
      <img
        data-speed="1.05"
        src="/images/decor-blue-rounded.png"
        className="offer__float offer__float--blue-border"
      />
      <img
        data-speed="1.1"
        src="/images/decor-green-rounded.png"
        className="offer__float offer__float--green-border"
      />
      <img
        data-speed="1.15"
        src="/images/decor-green-rounded.png"
        className="offer__float offer__float--green-border-2"
      />
      <img
        data-speed="1.1"
        src="/images/decor-green.png"
        className="offer__float offer__float--green"
      />

      <div className="offer__content">
        <h2 className="offer__title">What we offer?</h2>
        <div className="offer__cards">
          {offer.map((offerCard, index) => (
            <OfferCard
              className="offer__card"
              key={index}
              name={offerCard.name}
              icon={offerCard.icon}
            />
          ))}
        </div>
      </div>
    </section>
  );
}

export default Offer;
