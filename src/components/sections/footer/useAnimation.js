import { useEffect, useRef } from "react";
import gsap from "@/lib/gsap";
import ScrollTrigger from "@/lib/gsap/ScrollTrigger";
import SplitText from "@/lib/gsap/SplitText";
import useGlobal from "@/store/useGlobal.js";

gsap.registerPlugin(SplitText, ScrollTrigger);

const useAnimation = () => {
  const { isMobile } = useGlobal();

  const textRef = useRef(null);
  const splitTextRef = useRef(null);

  useEffect(() => {
    splitTextRef.current = new SplitText(textRef.current, { type: "lines" });

    const initBlock = (fast = true) => {
      const duration = fast ? 0 : 0.3;

      gsap.to(splitTextRef.current.lines, {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        textAlign: isMobile ? null : "left",
        duration,
      });

      gsap.to(".footer__button", {
        translateY: "100%",
        rotate: "5deg",
        opacity: 0,
        duration,
      });

      gsap.to(".footer__float", {
        opacity: 0,
        duration,
      });
    };

    initBlock(true);

    ScrollTrigger.create({
      trigger: ".footer__title",
      start: "top 80%",
      onLeaveBack: () => {
        initBlock(false);
      },
      onEnter() {
        gsap.to(".footer__float", {
          opacity: 1,
          duration: 0.3,
          stagger: 0.1,
        });

        const timeline = gsap.timeline();

        splitTextRef.current.lines.forEach((line) => {
          timeline.to(
            line,
            { translateY: "0%", rotate: "0deg", opacity: 1, duration: 0.3 },
            "<0.1"
          );
        });

        document.querySelectorAll(".footer__button").forEach((button) => {
          timeline.to(
            button,
            { translateY: "0%", rotate: "0deg", opacity: 1, duration: 0.3 },
            "<0.05"
          );
        });
      },
    });
  }, []);

  return { textRef };
};

export default useAnimation;
