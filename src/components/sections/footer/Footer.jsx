import React from "react";
import "./footer.scss";
import useAnimation from "@/components/sections/footer/useAnimation.js";
import useGlobal from "@/store/useGlobal.js";

function Footer() {
  const { textRef } = useAnimation();
  const { isMobile } = useGlobal();

  return (
    <footer className="footer">
      <div className="container footer__container">
        <img
          data-speed="1.1"
          src="/images/decor-blue-rounded.png"
          className="footer__float footer__float--blue-border"
        />
        <img
          data-speed="1.05"
          src="/images/decor-green-rounded.png"
          className="footer__float footer__float--green-border"
        />

        <div className="footer__title footer__title--desktop">
          <h2 className="title title--white title--big" ref={textRef}>
            {isMobile ? (
              <>
                <u className="title__underline">Want to know</u> more?
              </>
            ) : (
              <>
                Want to<u className="title__underline">know more?</u>
              </>
            )}
          </h2>
        </div>

        <div className="footer__buttons">
          <a
            href="https://prescriby.com"
            target="_blank"
            className="footer__button"
          >
            <span className="footer__button-inner">Our story</span>
          </a>
          <a
            href="https://prescriby.com/contact"
            target="_blank"
            className="footer__button"
          >
            <span className="footer__button-inner">Contact</span>
          </a>
          <a
            href="https://prescriby.com/aboutus"
            target="_blank"
            className="footer__button"
          >
            <span className="footer__button-inner">Create plan</span>
          </a>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
