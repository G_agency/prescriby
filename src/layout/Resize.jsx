import React, { useEffect, useMemo, useState } from "react";
import classNames from "class-names";
import PropTypes from "prop-types";
import useGlobal from "@/store/useGlobal.js";

const defaultFont = 16;

const desktopConfig = {
  defaultWidth: 1366,
  minWidth: 768,
  defaultHeight: 768,
  minHeight: 0,
};

const mobileConfig = {
  defaultWidth: 390,
  minWidth: 280,
  defaultHeight: 0,
  minHeight: 200,
};

function Resize({ children }) {
  const { setIsMobile, isMobile: isMobileGlobal } = useGlobal();
  const [viewPort, setViewPort] = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  const onResize = () => {
    const { innerWidth, innerHeight } = window;

    setViewPort({
      width: innerWidth,
      height: innerHeight,
    });

    const vh = innerHeight * 0.01;
    document.documentElement.style.setProperty("--vh", `${vh}px`);
  };

  useEffect(() => {
    window.addEventListener("resize", onResize);
    onResize();

    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  const isMobile = useMemo(() => {
    const status = viewPort.width < desktopConfig.minWidth;

    if (status !== isMobileGlobal) setIsMobile(status);

    return status;
  }, [viewPort.width]);

  const fontSize = (() => {
    const config = isMobile ? mobileConfig : desktopConfig;

    const horizontalRatio =
      Math.max(config.minWidth, viewPort.width) / config.defaultWidth;
    const verticalRatio =
      Math.max(config.minHeight, viewPort.height) / config.defaultHeight;
    const minRatio = Math.min(horizontalRatio, verticalRatio);

    return defaultFont * minRatio;
  })();

  return (
    <div
      style={{ fontSize: fontSize + "px" }}
      className={classNames({
        "is-mobile": isMobile,
        "is-desktop": !isMobile,
      })}
    >
      {children}
    </div>
  );
}

Resize.propTypes = {
  children: PropTypes.node,
};

export default Resize;
