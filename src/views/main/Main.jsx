import React from "react";
import Head from "@/components/sections/head/Head.jsx";
import AddictionBeginning from "@/components/sections/addictionBeginning/AddictionBeginning.jsx";
import AddictionPrevent from "@/components/sections/addictionPrevent/AddictionPrevent.jsx";
import Offer from "@/components/sections/offer/Offer.jsx";
import Partners from "@/components/sections/partners/Partners.jsx";
import Footer from "@/components/sections/footer/Footer.jsx";
import "./main.scss";

function Main() {
  return (
    <div className="main">
      <Head />
      <AddictionBeginning />
      <AddictionPrevent />
      <Offer />
      <Partners className="main__partners" />
      <Footer />
    </div>
  );
}

export default Main;
