import React, { useEffect, useRef, useState } from "react";
import Resize from "@/layout/Resize.jsx";
import { RouterProvider } from "react-router-dom";
import router from "@/router";
import "./app.scss";

import gsap from "@/lib/gsap";
import ScrollSmoother from "@/lib/gsap/ScrollSmoother";
import ScrollTrigger from "@/lib/gsap/ScrollTrigger";
import useGlobal from "@/store/useGlobal.js";
import Preloader from "@/components/preloader/Preloader.jsx";
import classNames from "class-names";
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock";

gsap.registerPlugin(ScrollSmoother, ScrollTrigger);

function App() {
  const { isMobile, isLoaded, setIsLoaded } = useGlobal();
  const appRef = useRef(null);
  const [update, setUpdate] = useState(false);

  useEffect(() => {
    // if (isMobile || isMobile === null) return;

    ScrollSmoother.create({
      wrapper: "#wrapper",
      content: "#content",
      smooth: 2,
      effects: true,
    });

    disableBodyScroll(appRef.current);
  }, []);

  const onLoaded = () => {
    setTimeout(
      () => {
        setIsLoaded(true);
        enableBodyScroll(appRef.current);
      },
      isMobile ? 0 : 500
    );
  };

  useEffect(() => {
    setUpdate(true);

    setTimeout(() => {
      setUpdate(false);
    }, 0);
  }, [isMobile]);

  return (
    <Resize>
      <div className="app" ref={appRef}>
        <Preloader
          loaded={onLoaded}
          className={classNames("app__preloader", {
            "app__preloader--loaded": isLoaded,
          })}
        />
        <div id="wrapper">
          <div id="content">
            {!update && <RouterProvider router={router} />}
          </div>
        </div>
      </div>
    </Resize>
  );
}

export default App;
