/*!
 * ScrollSmoother 3.11.1
 * https://greensock.com
 *
 * @license Copyright 2022, GreenSock. All rights reserved.
 * *** DO NOT DEPLOY THIS FILE ***
 * This is a trial version that only works locally and on domains like codepen.io and codesandbox.io.
 * Loading it on an unauthorized domain violates the license and will cause a redirect.
 * Get the unrestricted file by joining Club GreenSock at https://greensock.com/club
 * @author: Jack Doyle, jack@greensock.com
 */

let e,
  t,
  r,
  o,
  n,
  s,
  i,
  l,
  a,
  c,
  h,
  d,
  g,
  f = () => "undefined" != typeof window,
  u = () => e || (f() && (e = window.gsap) && e.registerPlugin && e),
  p = function () {
    return String.fromCharCode.apply(null, arguments);
  },
  m = p(103, 114, 101, 101, 110, 115, 111, 99, 107, 46, 99, 111, 109),
  v = (e) => {
    let t = o.querySelector(".ScrollSmoother-wrapper");
    return (
      t ||
        ((t = o.createElement("div")),
        t.classList.add("ScrollSmoother-wrapper"),
        e.parentNode.insertBefore(t, e),
        t.appendChild(e)),
      t
    );
  };
class y {
  constructor(f) {
    t ||
      y.register(e) ||
      console.warn("Please gsap.registerPlugin(ScrollSmoother)"),
      (f = this.vars = f || {}),
      c && c.kill(),
      (c = this);
    let u,
      p,
      m,
      w,
      S,
      b,
      T,
      x,
      E,
      k,
      C,
      P,
      R,
      A,
      {
        smoothTouch: _,
        onUpdate: z,
        onStop: F,
        smooth: L,
        onFocusIn: M,
        normalizeScroll: O,
      } = f,
      B = this,
      I =
        "undefined" != typeof ResizeObserver &&
        new ResizeObserver(() => w.refresh()),
      H = f.effectsPrefix || "",
      N = a.getScrollFunc(r),
      U =
        1 === a.isTouch
          ? !0 === _
            ? 0.8
            : parseFloat(_) || 0
          : 0 === L || !1 === L
          ? 0
          : parseFloat(L) || 0.8,
      q = 0,
      V = 0,
      W = 1,
      D = d(0),
      Y = () => D.update(-q),
      j = { y: 0 },
      K = () => (u.style.overflow = "visible"),
      G = (e) => {
        e.update();
        let t = e.getTween();
        t && (t.pause(), (t._time = t._dur), (t._tTime = t._tDur)),
          (R = !1),
          e.animation.progress(e.progress, !0);
      },
      J = (e, t) => {
        ((e !== q && !k) || t) &&
          (U &&
            ((u.style.transform =
              "matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, " +
              e +
              ", 0, 1)"),
            (u._gsap.y = e + "px")),
          (V = e - q),
          (q = e),
          a.isUpdating || a.update());
      },
      Q = function (e) {
        return arguments.length
          ? (e < 0 && (e = 0),
            (j.y = -e),
            (R = !0),
            k ? (q = -e) : J(-e),
            N(e),
            this)
          : -q;
      },
      X = (e) => {
        (p.scrollTop = 0),
          (e.target.contains && e.target.contains(p)) ||
            (M && !1 === M(this, e)) ||
            (a.isInViewport(e.target) ||
              e.target === A ||
              this.scrollTo(e.target, !1, "center center"),
            (A = e.target));
      },
      Z = (t, r) => {
        let o, n, s, i;
        S.forEach((r) => {
          (o = r.pins),
            (i = r.markers),
            t.forEach((t) => {
              r.trigger &&
                t.trigger &&
                r !== t &&
                (t.trigger === r.trigger ||
                  t.pinnedContainer === r.trigger ||
                  r.trigger.contains(t.trigger)) &&
                ((n = t.start),
                (s = (n - r.start - r.offset) / r.ratio - (n - r.start)),
                o.forEach((e) => (s -= e.distance / r.ratio - e.distance)),
                t.setPositions(n + s, t.end + s),
                t.markerStart &&
                  i.push(
                    e.quickSetter([t.markerStart, t.markerEnd], "y", "px")
                  ),
                t.pin &&
                  t.end > 0 &&
                  ((s = t.end - t.start),
                  o.push({ start: t.start, end: t.end, distance: s, trig: t }),
                  r.setPositions(r.start, r.end + s),
                  r.vars.onRefresh(r)));
            });
        });
      },
      $ = () => {
        K(),
          requestAnimationFrame(K),
          S &&
            (S.forEach((e) => {
              let t = e.start,
                r = e.auto
                  ? Math.min(a.maxScroll(e.scroller), e.end)
                  : t + (e.end - t) / e.ratio,
                o = (r - e.end) / 2;
              (t -= o),
                (r -= o),
                (e.offset = o || 1e-4),
                (e.pins.length = 0),
                e.setPositions(Math.min(t, r), Math.max(t, r)),
                e.vars.onRefresh(e);
            }),
            Z(a.sort())),
          D.reset();
      },
      ee = () => a.addEventListener("refresh", $),
      te = () => S && S.forEach((e) => e.vars.onRefresh(e)),
      re = () => (S && S.forEach((e) => e.vars.onRefreshInit(e)), te),
      oe = (e, t, r, o) => () => {
        let n = "function" == typeof t ? t(r, o) : t;
        return (
          n ||
            0 === n ||
            (n = o.getAttribute("data-" + H + e) || ("speed" === e ? 1 : 0)),
          o.setAttribute("data-" + H + e, n),
          "auto" === n ? n : parseFloat(n)
        );
      },
      ne = (t, o, s, i) => {
        let c,
          d,
          g,
          f,
          u,
          m,
          v = oe("speed", o, i, t),
          y = oe("lag", s, i, t),
          w = e.getProperty(t, "y"),
          b = t._gsap,
          T = () => {
            (o = v()),
              (s = y()),
              (c = parseFloat(o) || 1),
              (g = "auto" === o),
              (u = g ? 0 : 0.5),
              f && f.kill(),
              (f =
                s &&
                e.to(t, { ease: h, overwrite: !1, y: "+=0", duration: s })),
              d && ((d.ratio = c), (d.autoSpeed = g));
          },
          x = () => {
            (b.y = w + "px"), b.renderTransform(1), T();
          },
          E = [],
          k = [],
          C = 0,
          P = (e) => {
            if (g) {
              x();
              let o = ((e, t) => {
                let o,
                  s,
                  i = e.parentNode || n,
                  l = e.getBoundingClientRect(),
                  a = i.getBoundingClientRect(),
                  c = a.top - l.top,
                  h = a.bottom - l.bottom,
                  d = (Math.abs(c) > Math.abs(h) ? c : h) / (1 - t),
                  g = -d * t;
                return (
                  d > 0 &&
                    ((o = a.height / (r.innerHeight + a.height)),
                    (s =
                      0.5 === o
                        ? 2 * a.height
                        : 2 *
                          Math.min(a.height, (-d * o) / (2 * o - 1)) *
                          (t || 1)),
                    (g += t ? -s * t : -s / 2),
                    (d += s)),
                  { change: d, offset: g }
                );
              })(t, l(0, 1, -e.start / (e.end - e.start)));
              (C = o.change), (m = o.offset);
            } else (C = (e.end - e.start) * (1 - c)), (m = 0);
            E.forEach((e) => (C -= e.distance * (1 - c))),
              e.vars.onUpdate(e),
              f && f.progress(1);
          };
        return (
          T(),
          (1 !== c || g || f) &&
            ((d = a.create({
              trigger: g ? t.parentNode : t,
              scroller: p,
              scrub: !0,
              refreshPriority: -999,
              onRefreshInit: x,
              onRefresh: P,
              onKill: (e) => {
                let t = S.indexOf(e);
                t >= 0 && S.splice(t, 1), x();
              },
              onUpdate: (t) => {
                let r,
                  o,
                  n,
                  s = w + C * (t.progress - u),
                  i = E.length,
                  l = 0;
                if (t.offset) {
                  if (i) {
                    for (o = -q, n = t.end; i--; ) {
                      if (
                        ((r = E[i]),
                        r.trig.isActive || (o >= r.start && o <= r.end))
                      )
                        return void (
                          f &&
                          ((r.trig.progress +=
                            r.trig.direction < 0 ? 0.001 : -0.001),
                          r.trig.update(0, 0, 1),
                          f.resetTo("y", parseFloat(b.y), -V, !0),
                          W && f.progress(1))
                        );
                      o > r.end && (l += r.distance), (n -= r.distance);
                    }
                    s =
                      w +
                      l +
                      C *
                        ((e.utils.clamp(t.start, t.end, o) - t.start - l) /
                          (n - t.start) -
                          u);
                  }
                  (a = s + m),
                    (s = Math.round(1e5 * a) / 1e5 || 0),
                    k.length && !g && k.forEach((e) => e(s - l)),
                    f
                      ? (f.resetTo("y", s, -V, !0), W && f.progress(1))
                      : ((b.y = s + "px"), b.renderTransform(1));
                }
                var a;
              },
            })),
            P(d),
            (e.core.getCache(d.trigger).stRevert = re),
            (d.startY = w),
            (d.pins = E),
            (d.markers = k),
            (d.ratio = c),
            (d.autoSpeed = g),
            (t.style.willChange = "transform")),
          d
        );
      };
    function se() {
      return (
        (m = u.clientHeight),
        (u.style.overflow = "visible"),
        (s.style.height = m + "px"),
        m - r.innerHeight
      );
    }
    ee(),
      a.addEventListener("killAll", ee),
      e.delayedCall(0.5, () => (W = 0)),
      (this.scrollTop = Q),
      (this.scrollTo = (t, o, n) => {
        let s = e.utils.clamp(
          0,
          a.maxScroll(r),
          isNaN(t) ? this.offset(t, n) : +t
        );
        o
          ? k
            ? e.to(this, {
                duration: U,
                scrollTop: s,
                overwrite: "auto",
                ease: h,
              })
            : N(s)
          : Q(s);
      }),
      (this.offset = (t, r) => {
        let o,
          n = (t = i(t)[0]).style.cssText,
          s = a.create({ trigger: t, start: r || "top top" });
        return (
          S && Z([s]),
          (o = s.start),
          s.kill(!1),
          (t.style.cssText = n),
          (e.core.getCache(t).uncache = 1),
          o
        );
      }),
      (this.content = function (t) {
        if (arguments.length) {
          let r = i(t || "#smooth-content")[0] || s.children[0];
          return (
            r !== u &&
              ((u = r),
              (E = u.getAttribute("style") || ""),
              I && I.observe(u),
              e.set(u, {
                overflow: "visible",
                width: "100%",
                boxSizing: "border-box",
                y: "+=0",
              }),
              U || e.set(u, { clearProps: "transform" })),
            this
          );
        }
        return u;
      }),
      (this.wrapper = function (t) {
        return arguments.length
          ? ((p = i(t || "#smooth-wrapper")[0] || v(u)),
            (x = p.getAttribute("style") || ""),
            se(),
            e.set(
              p,
              U
                ? {
                    overflow: "hidden",
                    position: "fixed",
                    height: "100%",
                    width: "100%",
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                  }
                : {
                    overflow: "visible",
                    position: "relative",
                    width: "100%",
                    height: "auto",
                    top: "auto",
                    bottom: "auto",
                    left: "auto",
                    right: "auto",
                  }
            ),
            this)
          : p;
      }),
      (this.effects = (e, t) => {
        if ((S || (S = []), !e)) return S.slice(0);
        (e = i(e)).forEach((e) => {
          let t = S.length;
          for (; t--; ) S[t].trigger === e && S[t].kill();
        }),
          (t = t || {});
        let r,
          o,
          { speed: n, lag: s } = t,
          l = [];
        for (r = 0; r < e.length; r++) (o = ne(e[r], n, s, r)), o && l.push(o);
        return S.push(...l), l;
      }),
      (this.sections = (e, t) => {
        if ((b || (b = []), !e)) return b.slice(0);
        let r = i(e).map((e) =>
          a.create({
            trigger: e,
            start: "top 120%",
            end: "bottom -20%",
            onToggle: (t) => {
              (e.style.opacity = t.isActive ? "1" : "0"),
                (e.style.pointerEvents = t.isActive ? "all" : "none");
            },
          })
        );
        return t && t.add ? b.push(...r) : (b = r.slice(0)), r;
      }),
      this.content(f.content),
      this.wrapper(f.wrapper),
      (this.render = (e) => J(e || 0 === e ? e : q)),
      (this.getVelocity = () => D.getVelocity(-q)),
      a.scrollerProxy(p, {
        scrollTop: Q,
        scrollHeight: () => se() && s.scrollHeight,
        fixedMarkers: !1 !== f.fixedMarkers && !!U,
        content: u,
        getBoundingClientRect: () => ({
          top: 0,
          left: 0,
          width: r.innerWidth,
          height: r.innerHeight,
        }),
      }),
      a.defaults({ scroller: p });
    let ie = a.getAll().filter((e) => e.scroller === r || e.scroller === p);
    ie.forEach((e) => e.revert(!0)),
      (w = a.create({
        animation: e.fromTo(
          j,
          { y: 0 },
          {
            y: () => -se(),
            immediateRender: !1,
            ease: "none",
            data: "ScrollSmoother",
            duration: 100,
            onUpdate: function () {
              if (this._dur) {
                let e = R;
                e && (G(w), (j.y = q)), J(j.y, e), Y(), z && !k && z(B);
              }
            },
          }
        ),
        onRefreshInit: () => {
          if (S) {
            let e = a.getAll().filter((e) => !!e.pin);
            S.forEach((t) => {
              t.vars.pinnedContainer ||
                e.forEach((e) => {
                  if (e.pin.contains(t.trigger)) {
                    let r = t.vars;
                    (r.pinnedContainer = e.pin),
                      (t.vars = null),
                      t.init(r, t.animation);
                  }
                });
            });
          }
          (P = q), (j.y = p.scrollTop = 0);
        },
        id: "ScrollSmoother",
        scroller: r,
        invalidateOnRefresh: !0,
        start: 0,
        refreshPriority: -9999,
        end: se,
        onScrubComplete: () => {
          D.reset(), F && F(this);
        },
        scrub: U || !0,
        onRefresh: (t) => {
          G(t),
            (j.y = -N()),
            J(j.y),
            W || t.animation.progress(e.utils.clamp(0, 1, P / -t.end));
        },
      })),
      (this.smooth = function (e) {
        return (
          arguments.length && (U = e || 0),
          arguments.length
            ? w.scrubDuration(e)
            : w.getTween()
            ? w.getTween().duration()
            : 0
        );
      }),
      w.getTween() && (w.getTween().vars.ease = f.ease || h),
      (this.scrollTrigger = w),
      f.effects &&
        this.effects(
          !0 === f.effects
            ? "[data-" + H + "speed], [data-" + H + "lag]"
            : f.effects,
          {}
        ),
      f.sections &&
        this.sections(!0 === f.sections ? "[data-section]" : f.sections),
      ie.forEach((e) => {
        (e.vars.scroller = p), e.init(e.vars, e.animation);
      }),
      (this.paused = function (e, t) {
        return arguments.length
          ? (!!k !== e &&
              (e
                ? (w.getTween() && w.getTween().pause(),
                  N(-q),
                  D.reset(),
                  (C = a.normalizeScroll()),
                  C && C.disable(),
                  (k = a.observe({
                    preventDefault: !0,
                    type: "wheel,touch,scroll",
                    debounce: !1,
                    allowClicks: !0,
                    onChangeY: () => Q(-q),
                  })),
                  (k.nested = g(n, "wheel,touch,scroll", !0, !1 !== t)))
                : (k.nested.kill(),
                  k.kill(),
                  (k = 0),
                  C && C.enable(),
                  (w.progress = (-q - w.start) / (w.end - w.start)),
                  G(w))),
            this)
          : !!k;
      }),
      (this.kill = () => {
        this.paused(!1), G(w), w.kill();
        let e = (S || []).concat(b || []),
          t = e.length;
        for (; t--; ) e[t].kill();
        a.scrollerProxy(p),
          a.removeEventListener("killAll", ee),
          a.removeEventListener("refresh", $),
          s.style.removeProperty("height"),
          (p.style.cssText = x),
          (u.style.cssText = E);
        let o = a.defaults({});
        o && o.scroller === p && a.defaults({ scroller: r }),
          this.normalizer && a.normalizeScroll(!1),
          clearInterval(T),
          (c = null),
          I && I.disconnect(),
          r.removeEventListener("focusin", X);
      }),
      (this.refresh = (e, t) => w.refresh(e, t)),
      O &&
        (this.normalizer = a.normalizeScroll(
          !0 === O ? { debounce: !0, content: !U && u } : O
        )),
      a.config(f),
      "overscrollBehavior" in r.getComputedStyle(s) &&
        e.set([s, n], { overscrollBehavior: "none" }),
      "scrollBehavior" in r.getComputedStyle(s) &&
        e.set([s, n], { scrollBehavior: "auto" }),
      r.addEventListener("focusin", X),
      (T = setInterval(Y, 250)),
      "loading" === o.readyState || requestAnimationFrame(() => a.refresh());
  }
  get progress() {
    return this.scrollTrigger ? this.scrollTrigger.animation._time / 100 : 0;
  }
  static register(c) {
    return (
      t ||
        ((e = c || u()),
        f() &&
          window.document &&
          ((r = window), (o = document), (n = o.documentElement), (s = o.body)),
        e &&
          ((i = e.utils.toArray),
          (l = e.utils.clamp),
          (h = e.parseEase("expo")),
          (a = e.core.globals().ScrollTrigger),
          e.core.globals("ScrollSmoother", y),
          s &&
            a &&
            ((d = a.core._getVelocityProp),
            (g = a.core._inputObserver),
            (y.refresh = a.refresh),
            (t = 1)))),
      t
    );
  }
}
(y.version = "3.11.1"),
  (y.create = (e) =>
    c && e && c.content() === i(e.content)[0] ? c : new y(e)),
  (y.get = () => c),
  u() && e.registerPlugin(y);
export default y;
export { y as ScrollSmoother };
